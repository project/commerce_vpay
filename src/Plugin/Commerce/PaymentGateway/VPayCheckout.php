<?php

namespace Drupal\commerce_vpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_vpay\VPayLibrary;

/**
 * Provides the VPay payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "vpay_payment",
 *   label = @Translation("vPay Payment"),
 *   display_label = @Translation("vPay Payment"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_vpay\PluginForm\VPayCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class VPayCheckout extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['service_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Web Service URL'),
      '#default_value' => $this->configuration['service_url'],
      '#required' => TRUE,
      '#description' => $this->t('Change to Dev (Test) / Production (Live) Service URL as per mode selected above.'),
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Id'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['merchant_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Key'),
      '#default_value' => $this->configuration['merchant_key'],
      '#required' => TRUE,
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['service_url'] = $values['service_url'];
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['merchant_key'] = $values['merchant_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $vpay_library                = new VPayLibrary();
    $paramlist                   = [];
    $txnid                       = $request->get('transaction_id');
    $oid                         = $order->id();
    $paramlist['status']         = $request->get('status');
    $paramlist['transaction_id'] = $request->get('transaction_id');
    $paramlist['bill_no']        = $request->get('bill_no');
    $paramlist['checksum']       = $request->get('checksum');
    $paramlist['amount']         = $request->get('amount');

    $resp_format = "<91>amount<92><91>transaction_id<92><91>status<92>";
    $resp_checksumstring = $vpay_library->getCheckSumString($paramlist, $resp_format);
    $valid_checksum = $vpay_library->verifyChecksum($paramlist['checksum'], $resp_checksumstring, $this->configuration['secret_key']);

    if ($valid_checksum) {
      if ($paramlist['status'] == 'Success') {
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        $payment = $payment_storage->create([
          'state' => 'completed',
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $this->parentEntity->id(),
          'order_id' => $order->id(),
          'test' => $this->getMode() == 'test',
          'remote_id' => $txnid,
          'remote_state' => $paramlist['status'],
          'authorized' => \Drupal::time()->getRequestTime(),
        ]);
        $payment->save();
        $this->messenger()->addStatus($this->t('Your payment was successful with Order id : @orderid and Transaction id : @transaction_id has been received at : @date', [
          '@orderid' => $oid,
          '@transaction_id' => $txnid,
          '@date' => date("d-m-Y H:i:s",
          \Drupal::time()->getRequestTime()),
        ]));
      }
      else {
        $this->messenger()->addError($this->t('Your payment with Order id : @orderid and Transaction id : @transaction_id failed at : @date', [
          '@orderid' => $oid,
          '@transaction_id' => $txnid,
          '@date' => date("d-m-Y H:i:s",
          \Drupal::time()->getRequestTime()),
        ]));
        throw new PaymentGatewayException('Invalid Transaction. Please try again.');
      }
    }
    else {
      $this->messenger()->addError($this->t('Illegal Tampering / Transaction detected. Checksum verification failed for Order id : @orderid and Transaction id : @transaction_id failed at : @date', [
        '@orderid' => $oid,
        '@transaction_id' => $txnid,
        '@date' => date("d-m-Y H:i:s",
        \Drupal::time()->getRequestTime()),
      ]));
      throw new PaymentGatewayException('Illegal Tampering detected. Checksum verification failed.');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addError($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
    throw new PaymentGatewayException('Payment cancelled by user.');
  }

}
