<?php

namespace Drupal\commerce_vpay\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_vpay\VPayLibrary;

/**
 * Provides checkout form when using VPay payment gateway.
 */
class VPayCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $vpay_library = new VPayLibrary();
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    $order = Order::load($order_id);
    $amount = round($payment->getAmount()->getNumber(), 2);
    $billing_profile = $order->getBillingProfile();
    $phone = $billing_profile->get('field_phone')->value;
    $email = $order->getEmail();
    $address = $order->getBillingProfile()->address->first();
    $paymode = 1;
    $desc = "APD donation through vPay";
    $udf1 = $address->getAddressLine1();
    $udf2 = $address->getLocality();
    $udf3 = $address->getAdministrativeArea();
    $udf4 = $address->getCountryCode();
    $udf5 = $address->getPostalCode();
    $name = $address->getGivenName();
    $service_url = $payment_gateway_plugin->getConfiguration()['service_url'];
    $merchant_id = $payment_gateway_plugin->getConfiguration()['merchant_id'];
    $merchant_key = $payment_gateway_plugin->getConfiguration()['merchant_key'];
    $secret_key = $payment_gateway_plugin->getConfiguration()['secret_key'];

    $redirect_url = $service_url;

    $callback_url = Url::FromRoute('commerce_payment.checkout.return', ['commerce_order' => $order_id, 'step' => 'payment'], ['absolute' => TRUE])->toString();
    $date = date("Y-m-d H:i:s");
    $paramList["datetime"] = $date;
    $paramList["partnerid"] = $merchant_id;
    $paramList["billno"] = $order_id;
    $paramList["phone"] = $phone;
    $paramList["email"] = $email;
    $paramList["name"] = $name;
    $paramList["amount"] = $amount;
    $paramList["paymentmode"] = $paymode;
    $paramList["description"] = $desc;
    $paramList["udf1"] = $udf1;
    $paramList["udf2"] = $udf2;
    $paramList["udf3"] = $udf3;
    $paramList["udf4"] = $udf4;
    $paramList["udf5"] = $udf5;
    $paramList["response_url"] = $callback_url;

    $format = "<91>amount<92><91>phone<92><91>partnerid<92><91>billno<92>";

    $checksumstring = $vpay_library->getCheckSumString($paramList, $format);
    $checksum = $vpay_library->calculateChecksum($secret_key, $checksumstring);

    $paramList['checksum'] = $checksum;
    $data = json_encode($paramList);

    $ch = curl_init($redirect_url);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'X-App-Token: ' . $merchant_key,
    ]);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    $first_iter = explode('transaction_url', $response);
    $second_iter = explode('"', $first_iter[1]);
    $url_trans = stripslashes($second_iter[2]);

    return $this->buildRedirectForm($form, $form_state, $url_trans, $paramList, 'post');
  }

}
