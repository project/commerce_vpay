<?php

namespace Drupal\commerce_vpay;

/**
 * Class to interact with VPay official PHP library.
 */
class VPayLibrary {

  /**
   * Utility method to calculate checksum.
   */
  public function calculateChecksum($secret_key, $all) {
    $hash = hash_hmac('sha256', $all, $secret_key);
    $checksum = $hash;
    return $checksum;
  }

  /**
   * Utility method to verify checksum.
   */
  public function verifyChecksum($checksum, $all, $secret) {
    $cal_checksum = VPayLibrary::calculateChecksum($secret, $all);
    $bool = 0;
    if ($checksum == $cal_checksum) {
      $bool = 1;
    }

    return $bool;
  }

  /**
   * Utility method to get checksum string.
   */
  public function getCheckSumString($paramArray, $format) {
    $format = str_replace("<92>", "", $format);
    $fields = explode("<91>", $format);
    $checkSumString = "";
    for ($i = 1; $i < count($fields); $i++) {
      $field = $fields[$i];
      $checkSumString .= "'" . $paramArray[$field] . "'";
    }
    return $checkSumString;
  }

}
