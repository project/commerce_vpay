# commerce_vpay
The Commerce vPay module implements vPay payment processing service
(www.vpaynow.com) for Drupal Commerce 2.x.

After the module is installed, proceed as below:

Go to commerce >> Configuration >> Payment gateways >> Add payment gateway

Enter Name e.g. vPay Payment
Select Plugin - vPay Payment
Display name e.g. vPay Payment

Mode: Test or Live

Besides, you need to enter Web Service URL, Merchant Id, Merchant Key and
Secret Key as provided by the vPay integration team.

Status: enabled and save.
